# volkagames job interview

## Description

This script is designed as a trial assignment on a DataEngineer position at Volka Games. This is a script for analyzing LTV metric in the context of players's cohorts.

<b>setup.sh</b> - performs installation. Creates virtual environment, installs required python packages, specifying credentials.<br>
<b>query.py</b> - contains sql query to retreive data.<br>
<b>process.py</b> - builds a reports itself.

## Installation

Just type following code in your console:

```
git clone git@gitlab.com:atercygnus/volkagames_job_interview.git ./trial
cd ./trial
source ./setup.sh
```

## Useage

```
process.py [-h] [--period [PERIOD]] [--max_periods [MAX_PERIODS]]
                  start_date end_date

positional arguments:
  start_date            Starting day of report
  end_date              Ending day of report

optional arguments:
  -h, --help            show this help message and exit
  --period [PERIOD]     Period of analysis. Can be hour/day/week/month
  --max_periods [MAX_PERIODS]
                        Maximum periods for analysis. Need to keep reports not
                        too wide
```
### Examples

Building of a report for four days.
```
$ python process.py 2021-06-25 2021-06-29
Connecting postgresql database...OK!
Retreaving data from db...OK!
Processing data...OK!
Completed! See report_revenue.csv and report_gross.csv for revenue and gross report accordingly.
$ cat report_revenue.csv | column -t -s, | less -S
installs    ltv   LTV-1  LTV-2  LTV-3  LTV-4  LTV-5
2021-06-25  2383  0.99   0.44   0.6    0.61   0.93   0.99
2021-06-26  1882  9.05   4.99   5.98   7.89   9.05
2021-06-27  1708  2.82   2.05   2.82   2.82
2021-06-28  1618  0.26   0.01   0.26
```

Building of a report for two months with a week discretion. Number of periods in the report as constarined by 6 weeks.
```
$ python process.py 2021-05-01 2021-07-01 --period week --max_periods 6
Connecting postgresql database...OK!
Retreaving data from db...OK!
Processing data...OK!
Completed! See report_revenue.csv and report_gross.csv for revenue and gross report accordingly.
$ cat report_revenue.csv | column -t -s, | less -S
installs    ltv    LTV-1  LTV-2  LTV-3  LTV-4  LTV-5  LTV-6
2021-04-26  8205   4.1    0.59   1.66   2.06   2.2    2.72   4.1
2021-05-03  7229   24.11  2.11   10.07  12.64  15.26  19.7   24.11
2021-05-10  7666   28.22  5.52   9.5    12.96  18.29  23.99  28.22
2021-05-17  8201   34.71  5.74   10.52  15.57  21.6   28.29  34.71
2021-05-24  9067   23.32  2.91   9.79   14.12  18.1   23.11  23.32
2021-05-31  8224   21.5   4.53   12.73  18.19  20.72  21.5
2021-06-07  8038   15.34  3.51   9.8    14.6   15.34
2021-06-14  11359  30.27  5.79   25.77  30.27
2021-06-21  13837  5.82   4.61   5.82
2021-06-28  2083   0.33   0.33
```

## Notes

Several columns in payment table seems to have inappropriate type. JSON in not the best way to store prices. Suppose to have 6 float columns instead of 4 JSON columns(gross_currency, gross_amount, revenue_currency, revenue_amount). This is better way, both in terms of speed and memory.
