import os
import datetime

import pandas as pd
import psycopg2
import argparse

from query import ltv_query


def argparse_init():

    parser = argparse.ArgumentParser()
    parser.add_argument('start_date', type=datetime.date.fromisoformat, help='Starting day of report')
    parser.add_argument('end_date', type=str, help='Ending day of report')
    parser.add_argument('--period', nargs='?', type=str, help='Period of analysis. Can be hour/day/week/month', default='day')
    parser.add_argument('--max_periods', nargs='?', type=int, help='Maximum periods for analysis. Need to keep reports not too wide', default=10_000)
    args = parser.parse_args()

    return args


def make_ltv_report(df, value_col):
    
    ltv_ = df[['regdate', 'ltv_group', value_col]]
    ltv_ = pd.pivot_table(ltv_, values=value_col, columns='ltv_group', index='regdate')
    ltv_.index.name = ''
    ltv_.columns = [f'LTV-{c}' for c in ltv_.columns]

    ltv_['ltv'] = ltv_.max(axis=1)

    res = pd.merge(ltv_, df[['regdate', 'installs']].drop_duplicates().set_index('regdate'), left_index=True, right_index=True)
    ltv_cols = sorted(res.columns.difference(['ltv', 'installs']), key=lambda x: int(x.replace('LTV-', '')))
    res = res[['installs', 'ltv', *ltv_cols]]
    
    return res


if __name__ == '__main__':

    args = argparse_init() 

    print('Connecting postgresql database...', end='')
    conn = psycopg2.connect(os.getenv('connstr'))
    print('OK!')
    cur = conn.cursor()

    ltv_query = ltv_query.format(startdate=args.start_date, enddate=args.end_date, period=args.period)

    print('Retreaving data from db...', end='')
    cur.execute(ltv_query)
    res = cur.fetchall()
    print('OK!')

    print('Processing data...', end='')
    ltv = pd.DataFrame(res, columns = ['regdate', 'ltv_group', 'date', 'installs', 'cumltv_rev', 'cumltv_gross'])
    ltv = ltv[ltv.ltv_group <= args.max_periods]

    make_ltv_report(ltv, 'cumltv_rev').round(2).to_csv('report_revenue.csv')
    make_ltv_report(ltv, 'cumltv_gross').round(2).to_csv('report_gross.csv')
    print('OK!')

    print('Completed! See report_revenue.csv and report_gross.csv for revenue and gross report accordingly.')

