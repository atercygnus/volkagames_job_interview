ltv_query = '''
with 
	eindex_rev as (
		select 
			paymentid pid,
			array_position(array_agg(e::text), '"EUR"') eur_index_rev
		from payment, json_array_elements(revenue_currency) e --implicit cross-join
		group by paymentid),
	eindex_gross as (
		select 
			paymentid pid,
			array_position(array_agg(e::text), '"EUR"') eur_index_gross
		from payment, json_array_elements(gross_currency) e --implicit cross-join
		group by paymentid)
select 
	t1.regdate,
	rank() over (partition by t1.regdate order by t1.paydate) as ltv_group,
	t1.paydate,
	installs,
	--revenue,
	--revenue/installs as ltv,
	sum(revenue/installs) over (partition by t1.regdate order by t1.paydate) as cumltv_rev,
	sum(gross/installs) over (partition by t1.regdate order by t1.paydate) as cumltv_gross
from
(select * from
(select distinct date_trunc('{period}', registertime) regdate from payment where registertime between '{startdate}' and '{enddate}') as t1,
(select distinct date_trunc('{period}', paymenttime) paydate from payment where paymenttime >= registertime) as t2
where regdate <= paydate
order by 1, 2) t1
left join
(select 
	date_trunc('{period}', registertime) as regdate, 
	date_trunc('{period}', paymenttime) as paydate,
	sum((revenue_amount ->> eindex_rev.eur_index_rev - 1)::real) as revenue,
	sum((revenue_amount ->> eindex_gross.eur_index_gross - 1)::real) as gross
from payment
left join eindex_rev on payment.paymentid = eindex_rev.pid
left join eindex_gross on payment.paymentid = eindex_gross.pid
where 1=1
	and (registertime between '{startdate}' and '{enddate}') 
	and (paymenttime >= registertime)
	and (istestpayment = 0)
	and (discardtime is null) -- not sure about difference between discardtime and 
	and (rollbacktime is null) -- rollbacktime but suppose both should not be considered
group by 1, 2) t2
on t1.regdate = t2.regdate and t1.paydate = t2.paydate
left join
(select 
	date_trunc('{period}', registertime) as regdate, 
	count(distinct playerid) as installs 
from player 
group by regdate) t3
on t1.regdate = t3.regdate;
'''
