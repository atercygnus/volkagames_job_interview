#!/bin/bash

rm -rf ./venv
python -m venv venv
source ./venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
echo Plase, copypaste db connect string:
read connstr
export connstr=$connstr

echo "Setup complete! Now use like this: python process.py START_DATE END_DATE [--period] [--max_periods]"
